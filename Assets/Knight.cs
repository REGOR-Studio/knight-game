﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Serialization;

public class Knight : MonoBehaviour
{
    public Transform weapon;
    public Transform swordHand;
    public Rigidbody rb;
    
    public void AttachWeapon()
    {
        weapon.parent = swordHand;
    }

    private void Start()
    {
        Application.targetFrameRate = 30;
    }

    void OnMove(InputValue value)
    {
        rb.velocity = value.Get<Vector2>();
    }
}